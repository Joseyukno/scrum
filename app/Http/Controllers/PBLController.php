<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scrum;
use App\Project;
class PBLController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Scrum $scrum)
    {
        request()->validate([
            'SBL' => ['required']
        ]);
        Scrum::find(request('SBL'))->update([
            "progress" => "todo"
        ]);
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scrum = Scrum::all()->where('Scrum_project_id', $id)->where('progress', 'PBL');
        return view('/project/pbl/pbl', compact('id', 'scrum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        request()->validate([
            'item' => ['required', 'max:150'],
            'value' => ['required', 'max:4'],
            'title' => ['required', 'max:20'],
            'storypoints' => ['required', 'max:3'],
            'priority' => ['required'],
        ]);
        $scrum = new Scrum;
        $scrum->title = request('title');
        $scrum->info = request('item');
        $scrum->Scrum_project_id = $id;
        $scrum->progress = 'PBL';
        $scrum->priority = request('priority');
        $scrum->value = request('value');
        $scrum->storypoints = request('storypoints');
        $scrum->save();

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Scrum $scrum, $id)
    {
        request()->validate([
            'info' => ['required', 'max:150'],
            'value' => ['required', 'max:4'],
            'title' => ['required', 'max:20'],
            'storypoints' => ['required', 'max:3'],
            'priority' => ['required'],
        ]);
        Scrum::find($id)->update([
            "info" => request('info'),
            "value" => request('value'),
            "title" => request('title'),
            "storypoints" => request('storypoints'),
            "priority" => request('priority'),
        ]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Scrum::where('id', request('del'))->delete();
        return back();
    }
}
