@extends('layouts.scrum')

@section('content')
    @if(empty($project))
        @else
        @php
            if (trim(\App\Scrum::all()->where('Scrum_project_id', $project->id)->whereIn('progress', ['PBL','todo','doing','check','done','completed'])->pluck('storypoints'), '[]') == ''){
            $percentage = 0;
            } else{
               $notdone = \App\Scrum::all()->where('Scrum_project_id', $project->id)->whereIn('progress', ['PBL','todo','doing','check','done','completed'])->pluck('storypoints')->toArray();
                                $done = \App\Scrum::all()->where('Scrum_project_id', $project->id)->where('progress', 'completed')->pluck('storypoints')->toArray();
            $percentage = round(abs(100 / array_sum($notdone) * array_sum($done)));
            }
        @endphp
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            @if($project->owner_id == Auth::user()->id)
                    <h2   class="text-primary font-weight-bold m-0">
                        <form   method="POST" action="/project/{{ $project->id }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn-danger float-right  btn-sm">delete project</button>
                        </form>

                        <form style="display: inline-block;" method="POST" action="/project/{{ $project->id }}">
                            @csrf
                            @method('PATCH')
                            <input  class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                            type="text" name="projectname"
                            value="{{ $project->projectname }}">
                        <button  class="btn-primary  btn-sm" type="submit">Change Project Name</button>
                </form>
                    </h2>
                    <br/>
                    <div style="background-color: red;color: white; font-size: 20px;">@include('/errors/errors')</div>
            @else
                <h2 class="text-primary font-weight-bold m-0">{{ $project->projectname }}
                </h2>

            @endif

        </div>
        <div class="card-body">
            <h4 class="small font-weight-bold">{{ $project->projectname }} <span class="float-right">{{$percentage}}%</span>
            </h4>
            @if($percentage <= 25)
                <div class="progress progress-sm mb-3">
                    <div class="progress-bar bg-danger" aria-valuenow="{{$percentage}}"
                         aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: {{ $percentage}}%;"><span
                            class="sr-only">{{$percentage}}</span></div>
                </div>
            @elseif($percentage <= 45)
                <div class="progress progress-sm mb-3">
                    <div class="progress-bar bg-warning" aria-valuenow="{{$percentage}}"
                         aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: {{ $percentage}}%;"><span
                            class="sr-only">{{$percentage}}</span></div>
                </div>
            @elseif($percentage <= 70)
                <div class="progress progress-sm mb-3">
                    <div class="progress-bar bg-primary" aria-valuenow="{{$percentage }}"
                         aria-valuemin="0"
                         aria-valuemax="100"
                         style="width:{{$percentage}}%;"><span
                            class="sr-only">{{ $percentage }}</span></div>
                </div>
            @elseif($percentage <= 99)
                <div class="progress progress-sm mb-3">
                    <div class="progress-bar bg-info" aria-valuenow="{{$percentage }}"
                         aria-valuemin="0"
                         aria-valuemax="100"
                         style="width:{{$percentage }}%;"><span
                            class="sr-only">{{ $percentage }}</span></div>
                </div>
            @elseif($percentage == 100)
                <div class="progress progress-sm mb-3">
                    <div class="progress-bar bg-success" aria-valuenow="100"
                         aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: 100%;"><span class="sr-only">Completed</span>
                    </div>
                </div>
            @endif

            <div class="card-body">
                @if($project->owner_id == Auth::user()->id)
                    <a href="/pjm/{{ $project->id }}" class="small font-weight-bold">Project Managment<span
                            class="float-right">PJM</span></a>

                    <br/>
                @endif

                <a href="/dod/{{$project->id}}" class="small font-weight-bold">Definition of Done<span
                        class="float-right">DOD</span></a><br/>

                <a href="/pbl/{{$project->id}}" class="small font-weight-bold">Product Backlog<span class="float-right">PBL</span></a><br/>

                <a href="/sbl/{{$project->id}}" class="small font-weight-bold">Sprint Backlog<span class="float-right">SBL</span></a><br/>

                <a href="/scrumboard/{{$project->id}}" class="small font-weight-bold">Scrum Board<span class="float-right">SB</span></a><br/>

                <a href="/completed/{{$project->id}}" class="small font-weight-bold">Completed<span
                        class="float-right">DONE</span></a><br/>
            </div>


        </div>

        @if(count($project->users) > 1)
            <h2 style="margin-left: 40px;"><span>Your Scrumptious Scrumteam<img
                        class="rounded-circle mb-3 mt-4" src="/profiles/{{ Auth::user()->username }}/pf/pf.jpeg"
                        width="40" height="40"></span></h2>

        @endif
        <h4>
            @foreach($project->users as $user)
                @if($user->id == Auth::user()->id)
                @else
                    <span class="small font-weight-bold m-3 ">  {{ $user->username }}  <img
                            class="rounded-circle mb-3 mt-4"
                            src="/profiles/{{ $user->username }}/pf/pf.jpeg"
                            width="40" height="40"></span>
                @endif
            @endforeach

        </h4>
        <a href="/project" class=" font-weight-bold text-center">Back</a><br/>
    </div>
@endif
@endsection
