@extends('/layouts.scrum')
<title>Projects - Scrumptious</title>
@section('content')
    <div  class="card shadow mb-4" style="height: 560px; width: 100%; overflow-y: scroll;">
        <div class="card-header py-3" >
            <h6 class="text-primary font-weight-bold m-0">Definition of Done</h6>
        </div>

        <div style="width:99.999%; margin-left: 0.01%; font-size:20px;" class="row justify-content-center">
        <table class="ta" style="width:100%; font-size:18px; " >
            <tr>
                <th>delete</th>
                <th>DOD Title</th>
                <th>information</th>
            </tr>
            <form action="/dod/{{ $id }}" method="POST">
                @csrf
                @method('DELETE')
                @php
                    $count = 1;
                @endphp
                @foreach($scrum as $rule)

                    <tr>
                        <th><button name="del" value="{{ $rule->id }}" class="btn-primary  btn-sm">Delete</button></th>
                        <td>{{ $rule->title }}</td>
                        <td>{{ $rule->info }}</td>
                    </tr>


            @endforeach
        </table>
        </form>
        </div>
    </div>

 <div class="card shadow mb-4">
        <form method="GET" action="/dod/{{$id}}/edit" class="text-center">
            @csrf
            @if(empty($add))
                <h2 class="text-primary font-weight-bold m-0">Create a DOD Rule</h2>
                <h2 class="text-primary font-weight-bold m-0">
                    <input
                        class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                        type="text" name="title" style="width:200px;font-size:12pt;"
                        value="{{  old('title', '') }}"
                        placeholder="DOD Title">


                    <input
                        class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                        type="text" name="rule" style="width:600px;font-size:12pt;"
                        value="{{  old('rule', '') }}"
                        placeholder="User story DOD Rule">

                    <button class="btn-primary  btn-sm" type="submit">Add Rule</button>
                    @elseif(!empty($add))
                        <br/><br/>
                        <div style="background-color: @if($add == 'DOD Rule was added!') green; @else  red; @endif color: white; font-size: 20px;">{{$add ?? ''}}</div>
                    @endif
                </h2><br/>
                <div style="background-color: red;color: white; font-size: 20px;">@include('/errors/errors')</div>
                <a href="/project/{{$id}}" class=" font-weight-bold">Back</a><br/>

                <br/>

        </form>
    </div>


@endsection
