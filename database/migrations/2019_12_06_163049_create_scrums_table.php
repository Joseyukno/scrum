<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScrumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('Scrum_project_id');
            $table->string('title');
            $table->string('info');
            $table->integer('priority')->default('1');
            $table->integer('storypoints')->default(0);
            $table->integer('value')->default(0);
            $table->string('progress');
            $table->timestamps();

            $table->foreign('Scrum_project_id')
                ->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scrums');
    }
}
