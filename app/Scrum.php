<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scrum extends Model
{
    protected $fillable = [
        'rule', 'info', 'value', 'storypoints', 'priority', 'progress', 'title'
    ];
}
