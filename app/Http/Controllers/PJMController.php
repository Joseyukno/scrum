<?php
// project managment
namespace App\Http\Controllers;

use App\Project;
use App\Scrum;
use Illuminate\Http\Request;
use App\User;
use App\Group;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Routing\Loader\AnnotationDirectoryLoader;

class PJMController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, User $user, Project $project, Group $group)
    {
        $team = Project::find($id);
        if (Auth::user()->id == $team->owner_id){
            return view('/project/pjm/pjm', compact('id', 'team'));
        }
        else{
            header("refresh:0; url=/project/{$id}");
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Scrum $scrum
     * @return \Illuminate\Http\Response
     */
    public function show(Scrum $scrum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Scrum $scrum
     * @return \Illuminate\Http\Response
     */
    public function edit(Scrum $scrum,$id, Project $project, Group $group)
    {
        Project::where('id', $id)->update([
            'owner_id' => request('make')
        ]);
        header("refresh:0; url=/project/{$id}");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Scrum $scrum
     * @return \Illuminate\Http\Response
     */
    public function update($id, User $user, Group $group, Project $project)
    {
        $name = trim(User::all()->where('username', request('add'))->pluck('id'), '[]');
        $email = trim(User::all()->where('email', request('add'))->pluck('id'), '[]');
        $namecheck = trim(Group::all()->where('user_id', $name)->where('project_id', $id), '[]');
        $emailcheck = trim(Group::all()->where('user_id', $email)->where('project_id', $id), '[]');
        if (!empty($namecheck) || !empty($emailcheck)) {
            $add = 'this user is already in your group.';
        } elseif (!empty($name) || !empty($email)) {
            $add = new Group;
            if (!empty($name)) {
                $add->user_id = $name;
            } elseif (!empty($email)) {
                $add->user_id = $email;
            }
            $add->project_id = $id;
            $add->save();
            $add = 'user was added!';
        } else {
            $add = 'user was not found.';
        }
        $team = Project::find($id);
        header("refresh:3; url=/pjm/{$id}");
        return view("/project/pjm/pjm", compact('add', 'id', 'team'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Scrum $scrum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scrum $scrum, $id, User $user, Project $project, Group $group)
    {
        Group::where('project_id', $id)->where('user_id', request('remove'))->delete();
        return back();

    }
}
