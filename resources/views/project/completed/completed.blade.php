
@extends('/layouts.scrum')
<title>Projects - Scrumptious</title>
@section('content')


    <div class="card shadow mb-4" style="height: 560px; width: 100%; overflow-y: scroll;">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">Completed Items</h6>
        </div>
        <table class="ta" style="width:100%; font-size:18px;">
            <tr>
                <th>title</th>
                <th>Information</th>
                <th>B.value</th>
                <th>S.points</th>
                <th>Priority</th>
                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                <th>+PBL</th>
                <th>Delete</th>
                    @endif
            </tr>
            @foreach($scrum->sortbydesc('priority') as $rule)
                <td>
                    <div class="text-primary font-weight-bold m-0 btn-sm">
                        {{ $rule->title }}
                    </div>
                </td>
                <td>
                    <div class="text-primary font-weight-bold m-0 btn-sm">
                        {{ $rule->info }}
                    </div>
                </td>

                <td>
                    <div class="text-primary font-weight-bold m-0 btn-sm">
                        {{$rule->value}}
                    </div>
                </td>

                <td>
                    <div class="text-primary font-weight-bold m-0 btn-sm">
                        {{$rule->storypoints}}
                    </div>
                </td>
                <td>
                            @if($rule->priority == '1')
                                <button style="width: 90px;"  class="low" value="1">Low</button>
                            @elseif($rule->priority == '2')
                                <button style="width: 90px;" class="med" value="2">Medium</button>
                            @elseif($rule->priority == '3')
                                <button style="width: 90px;" class="high" value="3">High</button>
                            @else
                                <button style="width: 90px;" class="low" value="1">Low</button>
                            @endif
                </td>
                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                <td>
                    <form style="display: inline;" action="/sbl/{{ $id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button name="PBL" value="{{ $rule->id }}" class="btn-outline-warning btn-primary btn-sm"
                                style="color: white">BACK
                        </button>
                    </form>
                </td>
            <td>
                <form style="display: inline" action="/pbl/{{ $id }}" method="POST">
                @csrf
                @method('DELETE')
                    <button name="del" value="{{ $rule->id }}" class="btn-outline-danger btn-primary btn-sm"
                            style="color: white">Delete
                    </button>
                </form>
                </td>
                @endif

                </tr>
            @endforeach
        </table>
    </div>
    <a href="/project/{{$id}}" class="row justify-content-center font-weight-bold">Back</a><br/><br/>
@endsection
